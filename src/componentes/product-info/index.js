export const getProductList = () => {
    return new Promise((resolve, reject) => {
        window.fetch("http://localhost:8081/api/v1/products/", {
            method: "GET",
            mode: "cors",
            headers: {
                "content-type": "application/json;charset=UTF-8",
            },
        }).then(response => {
            resolve(response.json())
        }).catch(err => {
            reject(err);
        });
    })
}

