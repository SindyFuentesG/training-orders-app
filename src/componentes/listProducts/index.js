import React, { useState, useEffect } from "react";
import { getProductList } from '../product-info'

const ListProducts = () => {
    const [productList, setProductList] = useState([]);
    const [error, setError] = useState(false);
    const [load, setLoad] = useState(false);

    useEffect(() => {
        getProductList().then(response => {
            setProductList(response);
            setLoad(true)
        }).catch(err => {
            setError(true);
            setLoad(true)
            console.log(err);
        })
    }, []);

    return (
        <>
            {load ?
                (error ? (
                    <h2>Se presentó un error al consultar el listado de productos</h2>
                ) : (productList.length === 0 ? (
                    <h2>No se encontrarón pruductos creados</h2>
                ) : <pre>
                        <code>{JSON.stringify(productList, null, 2)}</code>
                    </pre>)
                ) : <h2>Cargando...</h2>}
        </>
    )
};

export default ListProducts;